package com.company;

import com.company.items.TypeAmiltonien;
import com.company.persons.*;
import com.company.tools.*;
import java.time.*;

public class Main {
    public static void main(String args[]){
      LocalDate date1 = LocalDate.of(2017,Month.APRIL,29);
      LocalDate date2 = LocalDate.of(2019,Month.MARCH,2);
      LocalDate date3 = LocalDate.of(2020,Month.DECEMBER,2);

      RessourcesHumaines RH = (RessourcesHumaines) AmiltonienFactory.getAmiltonien(TypeAmiltonien.RH, "TUTU","TATA" ,date3);

      assert RH != null;
      RH.addAmiltonien(TypeAmiltonien.SYS,"Raph","tata" ,date1);
      RH.addAmiltonien(TypeAmiltonien.DEV, "Daf" ,"leo",date2 );
      RH.addAmiltonien(TypeAmiltonien.RH,"Dupont" ,"Xavier",date3);

      System.out.println("****************************************************************************");

      System.out.println(RH.getAmiltoniens());

      System.out.println("****************************************************************************");
    }
}
