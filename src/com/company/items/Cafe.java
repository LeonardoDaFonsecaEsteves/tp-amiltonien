package com.company.items;

public class Cafe {
    private int nbSugar;
    private double fillingRate ;
    private double temperature;
    private boolean isHot = true;

    public Cafe(int sugar, double filling, double temp){
        this.nbSugar = sugar;
        this.fillingRate = filling;
        this.temperature = temp;
        if(temp > 50){
            setHot(true);
        }
    }

    public void setNbSugar(int nbSucre) {
        this.nbSugar = nbSucre;
    }

    public double getNbSugar() {
        return nbSugar;
    }

    public void setFillingRate(double fillingRate) {
        this.fillingRate = fillingRate;
    }

    public double getFillingRate() {
        return fillingRate;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
        if(temperature < 50)
            setHot(false);
    }

    public double getTemperature() {
        return temperature;
    }

    public void setHot(boolean hot) {
        this.isHot = hot;
    }

    public boolean isEmpty() {
        return isHot;
    }

    @Override
    public String toString() {
        String str = "\n----------------- \n Le Cafe à " + getNbSugar() +" sucre ,la tasse est remplie à "
                + getFillingRate()  +"%, est la temperature " + getTemperature() + "°C ☕☕☕ \n";
        if(isEmpty()) str += " Le cafe est trop chaud ♨♨♨ \n";
        return str;
    }


}
