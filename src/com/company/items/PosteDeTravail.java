package com.company.items;

public class PosteDeTravail {
    private boolean isBroken;
    private TypePoste poste;

    public PosteDeTravail(TypePoste poste){
        this.poste = poste;
        this.isBroken = false;
    }

    public boolean isBroken() {
        return isBroken;
    }

    public void setIsBroken(boolean casse) {
        isBroken = casse;
    }

    public TypePoste getPoste() {
        return poste;
    }

    public void setPoste(TypePoste poste) {
        this.poste = poste;
    }

    @Override
    public String toString() {
        String str ="Type de poste "+ getPoste() ;
        if(isBroken)
            str = str+" est le poste est casse!!!";

        return str;
    }

}
