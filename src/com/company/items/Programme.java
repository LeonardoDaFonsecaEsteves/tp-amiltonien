package com.company.items;

import com.company.tools.OutilsMathematiques;

public class Programme {
    private String namePrograme;
    private int nbBug;

    public Programme(String namePrograme){
        this.namePrograme = namePrograme;
        this.nbBug = OutilsMathematiques.getRandomNum();
    }

    public String getNamePrograme() {
        return namePrograme;
    }

    public void setNamePrograme(String namePrograme) {
        this.namePrograme = namePrograme;
    }

    public int getNbBug() {
        return nbBug;
    }

    public void setNbBug(int nbBug) {
        this.nbBug = nbBug;
    }

    @Override
    public String toString() {
        return "\nLe programme " + getNamePrograme() + '\'' +
                ", il à " + getNbBug()+" Bug(s)\n";
    }
}
