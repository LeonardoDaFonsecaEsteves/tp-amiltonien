package com.company.items;

public enum TypeAmiltonien {
    DEV ("Dev"),
    SYS ("Sys"),
    RH ("RH");

    private String type;

    TypeAmiltonien(String fonction) {
        this.type = fonction;
    }

    @Override
    public String toString() {
        return type;
    }
}
