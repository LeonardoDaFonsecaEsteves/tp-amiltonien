package com.company.items;

public enum TypePoste {
    F ("fixe"),
    P ("laptop");

    private String poste;

    TypePoste(String poste) {
        this.poste = poste;
    }

    @Override
    public String toString() {
        return poste;
    }
}
