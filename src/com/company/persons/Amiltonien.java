package com.company.persons;

import com.company.items.*;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;


public abstract class Amiltonien  {
    protected String nom;
    protected String prenom;
    protected LocalDate dateRecrutement;
    protected double nvMoral;
    protected Cafe cafe;

    ArrayList<Amiltonien> amiltoniens = new ArrayList<Amiltonien>();

    public Amiltonien(String nom, String prenom, LocalDate dateRecrutement) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateRecrutement = dateRecrutement;
        this.nvMoral  = 100;
        this.cafe = new Cafe(0,100,100);

    }

    public ArrayList<Amiltonien> getAmiltoniens() {
        return amiltoniens;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
        System.out.println(nom);
    }

    public Cafe getCafe() {
        return cafe;
    }

    public void setCafe(Cafe cafe) {
        this.cafe = cafe;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateRecrutement() {
        return dateRecrutement;
    }

    public void setDateRecrutement(LocalDate dateRecrutement) {
        this.dateRecrutement = dateRecrutement;
    }

    public double getNvMoral() {
        return nvMoral;
    }

    public void setNvMoral(double nvMoral) {
        this.nvMoral = nvMoral;
    }

    public void drinkCoffee(){
        if(this.cafe.getFillingRate() > 0) {
            double reste = (this.cafe.getFillingRate() * 90 / 100);
            this.cafe.setFillingRate(reste);
        }
        System.out.println(cafe.toString());
    }

    public void blow() {
        double R = this.cafe.getTemperature() / 2;
        this.cafe.setTemperature(R);
        System.out.println(cafe.toString());
    }

    public void compareAmiltonien(Amiltonien amiltonien) {
        if(this.dateRecrutement.isAfter(amiltonien.dateRecrutement)){
            System.out.println(this.nom+ ", "+this.prenom+ " est arriver apres "+amiltonien.nom+", "+amiltonien.prenom);
        } else {
            System.out.println(this.nom+ ", "+this.prenom+ " est arriver avant "+amiltonien.nom+", "+amiltonien.prenom);
        }
    }

    public void searchAmiltonien(String nom, String prenom){
        Stream<Amiltonien> amiltonienStream = this.amiltoniens.stream();
        Amiltonien a = amiltonienStream.filter(x -> x.nom == nom && x.prenom == prenom).findAny().orElse(null);
        System.out.println("Recherche " + nom+", "+prenom);
        if(a !=null){
            System.out.println(a);
       }else System.out.println("Aucune correspondance");

    }


    @Override
    public String toString() {
        return "Nom : "+getNom()+",\nPrenom : "+getPrenom()+",\nDate de contrat : "+getDateRecrutement()+",\nNiveau moral : "+getNvMoral()+"%,\n" + getCafe() ;
    }

}

