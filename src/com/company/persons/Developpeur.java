package com.company.persons;

import com.company.items.*;
import com.company.tools.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Developpeur extends Amiltonien {
    private PosteDeTravail postetravail;
    private List<Programme> listProgramme;
    private final String FAVORITE_LANG = "Java";

    public Developpeur(String nom, String prenom, LocalDate dateRecrutement)  {
        super(nom,prenom,dateRecrutement);
        this.listProgramme = new ArrayList<>();
        this.postetravail = new PosteDeTravail(TypePoste.F);
        if(!postetravail.isBroken()){
          createPrograme();
        }
    }

    public PosteDeTravail getPostetravail() {
        return postetravail;
    }

    public void setPostetravail(PosteDeTravail postetravail) {
        this.postetravail = postetravail;
    }

    /**
     * Methode pour cree des programe random
     */
    private void createPrograme() {
        int nb = OutilsMathematiques.getRandomNum();
        for(int i = 0 ; i < nb ; i++){
            listProgramme.add(new Programme("Amiltonien V"+i));
        }
    }

    public List<Programme> getListProgramme() {
        if(listProgramme.size() != 0 )
           return   listProgramme;
        else return null;
    }

    public void getLangagePrefere(){
        System.out.println("Son langage préféré ? Le "+FAVORITE_LANG+" évidemment !") ;
    }

    /**
     * methode pour casse le poste
     */
    public void brokenPost(){
        postetravail.setIsBroken(true);
        System.out.println(super.nom +" à casse sont poste ");
    }

    /**
     * @param nomPrograme = recoit le nom du programme à suprimer
     */
    public void deletePrograme(String nomPrograme){
        System.out.println("Avant "+listProgramme);
        listProgramme.remove(searchPrograme(nomPrograme));
        System.out.println("Apres "+listProgramme);
    }

    /**
     * @param nomPrograme = recoit le nom du programme a rechercher
     * @return renvoi le programe si existe sinon renvoi null
     */
    public Programme searchPrograme(String nomPrograme) {
        Programme ob = null;
      for (Programme src : listProgramme) {
            if (src.getNamePrograme().equals(nomPrograme))
               ob = src;
        }
      return ob;
    }

    /**
     * @param prg = programe a corigé
     */
    public void corrigeBug(Programme prg){
        System.out.println("Modifier  " +prg);
        int newNbBug = 0;
        String good = "";
        if(OutilsMathematiques.getInt() == 1){
            newNbBug = prg.getNbBug() + OutilsMathematiques.getMaxInt(prg.getNbBug());
            good = "👎👎👎";
        } else {
            newNbBug = prg.getNbBug() - OutilsMathematiques.getMaxInt(prg.getNbBug());
            good = "👍👍👍";
        }
        prg.setNbBug(newNbBug);
        System.out.println("Le programe a ete modifié " +prg+ " "+good );
    }

    @Override
    public String toString() {
        return "**********************\n Devloppeur \n-----------------\n"+ super.toString()
                +"\n-----------------\n travail sur un " +getPostetravail()+",\n est sont langage favorie "+FAVORITE_LANG
                +"\n-----------------\n Liste des programmes "+ getListProgramme() +"\n-----------------";
    }
}
