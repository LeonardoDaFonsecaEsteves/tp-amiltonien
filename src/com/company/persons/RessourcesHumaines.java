package com.company.persons;

import com.company.tools.*;
import com.company.items.*;
import java.time.LocalDate;

public class RessourcesHumaines extends Amiltonien {
    private PosteDeTravail poste;

    public RessourcesHumaines(String nom, String prenom, LocalDate dateRecrutement) {
        super(nom,prenom,dateRecrutement);
        this.poste = new PosteDeTravail(TypePoste.P);
    }

    /**
     * @param amiltonien
     * @return L'Amiltonien dont le moral a étè augmenté de 10% a condition qu'il soit inferieur a 90%
     */

    public Amiltonien upMoral(Amiltonien amiltonien){
        if(amiltonien.getNvMoral() < 90){
            amiltonien.setNvMoral(amiltonien.getNvMoral() + 10);
        }
        return amiltonien;
    }

    /**
     * @param fonction  recoit le type de poste dev rh sys
     * @param nom recoit le nom
     * @param prenom recoit le prenom
     * @param date recoit la date
     * @return Une liste avec le nouveau Amiltonien ajouté
     */

    public void addAmiltonien(TypeAmiltonien fonction, String nom, String prenom, LocalDate date){
       super.amiltoniens.add(AmiltonienFactory.getAmiltonien(fonction ,nom, prenom, date));
    }

    public String toString(){
        return "\nRessource Humaine \n"+ super.toString() + " est travail sur un poste "+ poste.getPoste()+"\n";
    }

}
