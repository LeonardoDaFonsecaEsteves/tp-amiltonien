package com.company.persons;

import com.company.items.*;
import java.time.LocalDate;

public class SysAdmin extends Amiltonien {
    private PosteDeTravail poste;

    public SysAdmin(String nom, String prenom, LocalDate dateRecrutement){
        super( nom,  prenom,  dateRecrutement);
        this.poste = new PosteDeTravail(TypePoste.F);
    }

    public void repairPoste(Developpeur dev){

       if(dev.getPostetravail().isBroken()){
        System.out.println( "Le poste de travail a étè réparair par "+ super.nom +", " + super.prenom+ " 😍😍😍");
           dev.getPostetravail().setIsBroken(false);
       } else {
          System.out.println("Le pc na pas besoin d'etre  😁😁😁 ");
       }
    }

    @Override
    public String toString(){
        return "\n**********************\n SysAdmin \n-----------------\n"+  super.toString()
                +"\n-----------------\n travail sur un poste "+poste.getPoste() + " est repare beaucoup de poste de travail \n";
    }
}
