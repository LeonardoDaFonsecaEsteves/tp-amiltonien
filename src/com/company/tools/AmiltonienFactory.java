package com.company.tools;

import com.company.items.TypeAmiltonien;
import com.company.persons.*;
import java.time.LocalDate;

public class AmiltonienFactory {
    /**
     * @param type
     * @param nom
     * @param prenom
     * @param dateRecrutement
     * @return
     */
    public static Amiltonien getAmiltonien(TypeAmiltonien type, String nom, String prenom, LocalDate dateRecrutement){
        return switch (String.valueOf(type)) {
            case "Dev" -> new Developpeur(nom, prenom, dateRecrutement);
            case "Sys" -> new SysAdmin(nom, prenom, dateRecrutement);
            case "RH" -> new RessourcesHumaines(nom, prenom, dateRecrutement);
            default -> null;
        };
    }
}
