package com.company.tools;

import java.util.Random;

public class OutilsMathematiques {

    /**
     * @return un nombre aleatior entre 0 est 10
     */

    public static int getRandomNum(){
        Random rn = new Random();
        int randomNum = rn.nextInt(10);
        return randomNum;
    }

    /**
     *
     * @return 0 ou 1
     */
    public static int getInt(){
        return (int) (Math.random()*2);
    }

    /**
     *
     * @param max
     * @return renvoie le nb max de bug
     */
    public static int getMaxInt(int max){
        return (int) (Math.random()*max);
    }


}
